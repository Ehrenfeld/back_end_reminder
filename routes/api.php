<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/reminders', 'ApiReminderController@index' );
Route::post('/reminders/create', 'ApiReminderController@store' );

Route::delete('/reminders/{id}/delete', 'ApiReminderController@destroy' );
Route::delete('/reminders/{id}/multidelete', 'ApiReminderController@multidestroy' );

Route::options('/reminders/{id}/multidelete', 'ApiReminderController@preflight' );

Route::post('/', 'ApiUserController@checkApi' );
Route::post('/users/create', 'ApiUserController@store' );
Route::post('/users/login', 'ApiUserController@login' );


