<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reminder extends Model
{
    protected $fillable = ['title', 'content', 'date_debut', 'date_fin', 'user_id'];

    function user() {

        return $this->belongsTo(User::class);
    }
}
