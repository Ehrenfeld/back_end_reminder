<?php

namespace App\Http\Controllers;

use App\Reminder;
use Illuminate\Http\Request;

class ApiReminderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $test = Reminder::all()->where('user_id', $request->user_id );

        return response( $test )
            ->header( 'Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'POST, GET');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $test = Reminder::create([
            'title' => $_POST['title'],
            'content' => $_POST['content'],
            'date_debut' => $_POST['date_debut'],
            'date_fin' => $_POST['date_fin'],
            'user_id' => $_POST['user_id']
        ]);

        return response( $test )
            ->header( 'Access-Control-Allow-Origin', '*')
            ->header( 'Access-Control-Allow-Methods', 'POST, GET');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function preflight()
    {
        return response( ['success' => true] )
            ->header( 'Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'DELETE');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = Reminder::destroy($id);

        return response( $response )
            ->header( 'Access-Control-Allow-Origin', '*');
    }

    public function multidestroy( $id ) {

        $ids = explode(",", $id);
        Reminder::whereIn('id', $ids)->delete();
    }
}
