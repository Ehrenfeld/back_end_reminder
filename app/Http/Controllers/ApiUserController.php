<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $test = User::find()
                ->where('password', $_POST['password'])
                ->and()
                ->where('username', $_POST['username']);


        return response( $test )
            ->header( 'Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'POST, GET');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function preflight()
    {
        return response( ['success' => true] )
            ->header( 'Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'POST');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    public function store(Request $request)
    {

        $test1 = DB::table('users')
            ->where([
                'username' => $request->username
            ])
            ->get();

        if ( $test1->isEmpty() ) {

            $test = User::create([
                'username' => $request->username,
                'password' => $request->password,
                'api_key' => $request->api_key
            ]);

            return response( $test )
                ->header( 'Access-Control-Allow-Origin', '*')
                ->header('Access-Control-Allow-Methods', 'POST, GET');
        }
    }

    public function login(Request $request)
    {
        $test = DB::table('users')
            ->where([
                'username' => $request->username,
                'password' => $request->password
            ])
            ->get('id');
           $test =  explode(':',(string)$test);
           $test = explode('}', $test[1]);

        if ( !empty($test) ) {
            $test0 = User::find(intval($test[0]));
            return response( $test0 )
                ->header( 'Access-Control-Allow-Origin', '*')
                ->header('Access-Control-Allow-Methods', 'POST, GET');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function checkApi(Request $request)
    {
        $test = DB::table('users')
            ->where('api_key', '=' , $request->api_key)
            ->where('id', '=' , $request->id)
            ->get();

        if ( !$test->isEmpty() ) {

            return response($test)
                ->header('Access-Control-Allow-Origin', '*')
                ->header('Access-Control-Allow-Methods', 'POST, GET');
        }
    }

}
